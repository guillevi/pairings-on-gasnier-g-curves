"""
Test pairings on Gasnier-Guillevic curve k=28 D=11 (GG28D11)
Author: Aurore Guillevic
Date: 2024, December 15

TODO: is p = 1 mod 7 possible?
"""
from sage.all_cmdline import *   # import sage library

from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.misc.functional import cyclotomic_polynomial
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.schemes.elliptic_curves.constructor import EllipticCurve

# this is much much faster with this statement:
# proof.arithmetic(False)
from sage.structure.proof.all import arithmetic

from pairing import *
from pairing_gg28d11 import *
from test_pairing import *
#from test_scalar_mult import test_glv_scalar_mult_g1_j_32768
#from cost_pairing import cost_pairing_gg28d11

def test_curve(u0, a=None, b=None):
    """
    - `u0`: integer seed
    - `a`: curve coefficient (in short Weierstrass form)
    - `b`: curve coefficient (in short Weierstrass form)
    """
    print("u0 = {:#x}".format(u0))
    #preparse("QQx.<x> = QQ[]")
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    # see also gitlab.inria.fr/tnfs-alpha/alpha sage/tnfs/curve/gg.py
    k = 28
    D = 11
    px = (x**30 + x**29 + 3*x**28 + 2515*x**16 + 14384*x**15 + 7545*x**14 + 4782969*x**2 + 13304911*x + 14348907)/38419953
    qx = px
    rx = (x**24 + 5*x**22 + 16*x**20 + 35*x**18 + 31*x**16 - 160*x**14 - 1079*x**12 - 1440*x**10 + 2511*x**8 + 25515*x**6 + 104976*x**4 + 295245*x**2 + 531441)/(3**12 * 13**2 * 83**2)
    tx = (x**15 + 718*x + 3237)/3237
    yx = (x**15 + 6*x**14 + 7192*x + 7545)/35607
    cx = (x**6 + x**5 - 2*x**4 - 5*x**3 - 6*x**2 + 9*x + 27)*3**11/11
    assert cx == (x**2 + x + 3) * (x**4 - 5*x**2 + 9) * 3**11/11
    lambrx = (x**14 + 718)/1079
    lambcx = (x**5 - 2*x**4 - 5*x**3 - 8*x**2 + 9*x + 18)/18
    lambx = (1079*x**29 - 718*x**28 + 2713685*x**15 + 7760168*x**14 + 5160823551*x + 3434171742)/(2 * 3**14 * 13 * 83)
    assert (lambx**2 + lambx + 3) % (rx*cx) == 0
    assert lambcx == lambx % cx
    assert lambrx == lambx % rx
    betax = (13*x**29 - 22*x**28 + 43*x**27 - 171*x**26 + 129*x**25 - 513*x**24 + 387*x**23 - 1539*x**22 + 1161*x**21 - 4617*x**20 + 3483*x**19 - 13851*x**18 + 10449*x**17 - 41553*x**16 + 64042*x**15 + 33902*x**14 - 153635*x**13 + 16413*x**12 - 460905*x**11 + 49239*x**10 - 1382715*x**9 + 147717*x**8 - 4148145*x**7 + 443151*x**6 - 12444435*x**5 + 1329453*x**4 - 37333305*x**3 + 3988359*x**2 - 49821318*x + 60313497)/64301926
    m = 35607
    u_mod_m = [5076, 9366, 13293, 17583]
    # D = 11 = 3 mod 4 -> Chi = x**2 + x + 3 has roots (-1+sqrt(-11))/2 and (-1 - sqrt(-11/))/2
    assert ((betax**2 + betax + 3) % px) == 0
    mux = -lambx-1
    assert ((lambx**2 + lambx + 3) % (rx*cx)) == 0
    assert ((mux**2 + mux + 3) % (rx*cx)) == 0
    assert ((lambrx**2 + lambrx + 3) % rx) == 0
    assert ((lambcx**2 + lambcx + 3) % cx) == 0
    chi = x**2 + x + 3

    print("u0 mod {} = {}".format(m, u0 % m))

    ### cofactor of G2 defined over GF(p^14)
    #ZZqty.<q,t,y> = ZZ[]
    #t2 = t*t - 2*q
    #t3 = t*t2 -q*t
    #t4 = t*t3 -q*t2
    #t5 = t*t4 -q*t3
    #t6 = t*t5 -q*t4
    #t7 = t*t6 -q*t5
    #t8 = t*t7 -q*t6
    #t9 = t*t8 -q*t7
    #t10 = t*t9 -q*t8
    #t11 = t*t10 -q*t9
    #t12 = t*t11 -q*t10
    #t13 = t*t12 -q*t11
    #t14 = t*t13 -q*t12
    #(q**14+1-t14).factor()
    #(t14**2 - 4*q**14).factor()
    tx2 = tx*tx - 2*qx
    tx14 = tx**14 - 14*qx*tx**12 + 77*qx**2*tx**10 - 210*qx**3*tx**8 + 294*qx**4*tx**6 - 196*qx**5*tx**4 + 49*qx**6*tx**2 - 2*qx**7
    assert (qx**14+1-tx14) % (qx+1-tx) == 0
    tx7 = tx**7 - 7*qx*tx**5 + 14*qx**2*tx**3 - 7*qx**3*tx
    assert (qx**14+1-tx14) % (qx**7+1-tx7) == 0
    assert (qx**14+1-tx14) % (qx**7+1+tx7) == 0
    assert (qx**14+1-tx14) == (qx**7+1+tx7)*(qx**7+1-tx7)
    #(t14**2 - 4*q**14).factor()
    # t^2 * (t^2 - 4*q) * (t^6 - 7*q*t^4 + 14*q^2*t^2 - 7*q^3)^2 * (t^6 - 5*q*t^4 + 6*q^2*t^2 - q^3)^2
    yx7 = yx * (tx**6 - 5*qx*tx**4 + 6*qx**2*tx**2 - qx**3)
    yx14 = yx7 * tx * (tx**6 - 7*qx*tx**4 + 14*qx**2*tx**2 - 7*qx**3)     
    E2_order = px**14 + 1 + tx14
    assert E2_order % (px**2 + 1 + tx2) == 0 # this is (q^2 + 1 + t2)
    assert ((E2_order // (px**2 + 1 + tx2)) % rx) == 0
    assert (E2_order % rx) == 0
    c2x = E2_order // rx # reducible with 3 factors
    c2xa = (x**4 - 5*x**2 + 9)/9
    c2xb = (x**56 + 2*x**55 + 12*x**54 + 16*x**53 + 60*x**52 + 62*x**51 + 192*x**50 + 166*x**49 + 420*x**48 + 272*x**47 + 372*x**46 - 134*x**45 - 1920*x**44 - 3118*x**43 - 7918*x**42 + 19414*x**41 + 36638*x**40 + 226526*x**39 + 299722*x**38 + 957904*x**37 + 1168868*x**36 + 2750786*x**35 + 3146842*x**34 + 5132794*x**33 + 5214398*x**32 + 906896*x**31 - 2249588*x**30 - 41660666*x**29 - 42286359*x**28 - 107938114*x**27 + 201704008*x**26 + 83997358*x**25 + 1301598020*x**24 + 1391429816*x**23 + 4692654028*x**22 + 6201172858*x**21 + 11748887960*x**20 + 18482995946*x**19 + 16510553548*x**18 + 36604424008*x**17 - 23187223900*x**16 + 16675156526*x**15 - 240472767362*x**14 - 41543878920*x**13 - 457531642036*x**12 + 62512296642*x**11 - 486635388062*x**10 + 686456393490*x**9 + 1684607838014*x**8 + 2869671297672*x**7 + 12802757682628*x**6 + 8170248946950*x**5 + 48852317871014*x**4 + 15024203055702*x**3 + 129036770211418*x**2 + 1588774755960*x + 228389782672925)/(11**2 * 13**4 * 83**4)
    c2xc = c2x//(c2xa*c2xb)
    ### final exp
    Tx = tx-1
    Phi_k = cyclotomic_polynomial(k)
    assert ((x**k-1) // Phi_k) == (x**14-1)*(x**2+1)
    h2x = Phi_k(Tx) // rx
    exponent_x = (px**12 - px**10 + px**8 - px**6 + px**4 - px**2 + 1)//rx
    exponent = ZZ(exponent_x(u0))
    #sx = 
    #s1 = ZZ(sx(u0))
    #expected_exp =  sx * exponent_x
    #eee = ZZ(expected_exp(u0))
    #assert eee == s1 * exponent
    #ee1 = 23**2 * eee

    p = ZZ(px(u0))
    r = ZZ(rx(u0))
    c = ZZ(cx(u0))
    c2 = ZZ(c2x(u0))
    t = ZZ(tx(u0))
    y = ZZ(yx(u0))
    Fp = GF(p, proof=False)
    t14 = ZZ(tx14(u0))
    assert (p**14 + 1 + t14) == c2 * r
    #if a is None or b is None:
    #    a, b, E = find_curve_parameter_ab(Fp, r, c)
    #else:
    j = -32768
    a_ = Fp(24)
    b_ = Fp(14)*sqrt(Fp(-11))
    assert Fp(1728*4*a**3/(4*a**3 + 27*b**2)) == Fp(-32768)
    #a = -24/11 -> * (-11) -> a = 24
    #b = -14/11 -> * (-11)*sqrt(-11) -> b = 14*sqrt(-11)

    if (Fp(-3/a_)).is_square():
        s1 = sqrt(Fp(-3/a_))
        a1 = -3
        b1 = b_ * s1**3
        E1 = EllipticCurve(Fp, [a1, b1])
        P = E1.random_element()
        if not r*c *P == E1(0) and not Fp(-1).is_square():
            b1 = -b1
            E1 = EllipticCurve(Fp, [a1, b1])
            P = E1.random_element()
            if r*c *P == E1(0):
                a = a1
                b = b1
        else:
            a = a1
            b = b1
    E = EllipticCurve(Fp, [a, b])

    if a == -3:
        print("new Gasnier k{}D{}-{} E: y^2 = x^3 {:+d}*x {:+d} /Fp of {} bits".format(k, D, p.nbits(), a, ZZ(b), p.nbits()))
    else:
        print("new Gasnier k{}D{}-{} E: y^2 = x^3 {:+d}*x {:+d} /Fp of {} bits".format(k, D, p.nbits(), a, b, p.nbits()))
    print("u = {:#x}".format(u0))
    print("p = {:#x} # {} bits, {} mod 11, {} mod k, log_2 p^{} = {:.2f}, {} bits".format(p, p.nbits(), p % 11, p % k, k, float((k*log(p)/log(2)).n()), ceil((k*log(p)/log(2)).n())))
    print("r = {:#x} # {} bits".format(r, r.nbits()))
    print("c = {:#x} # {} bits".format(c, c.nbits()))
    print("y = {:#x}".format(y))
    print("t = {:#x}".format(t))

    print("p = {} mod 4".format(p % 4))
    print("p-1 has 2-valuation {}".format((p-1).valuation(2)))
    print("r-1 has 2-valuation {}".format((r-1).valuation(2)))
    lambda_mod_r = ZZ(lambx(u0))
    beta_mod_p = Fp(betax(u0))

    Fpz = Fp['z']; (z,) = Fpz._first_ngens(1)
    # define Fp14
    d = 2 # quadratic twist
    e = k//d
    a0 = 1
    if (p % e) == 1:
        a1 = 0
    else:
        a1 = 1
    while not (z**e + a1*z - a0).is_irreducible():
        if a1 == 0: # binomial x^e - a0
            if a0 < 0:
                a0 = -a0 + 1
            else:
                a0 = -a0
        else:
            if abs(a0) < 2*abs(a1): # increment a0
                if a0 < 0:
                    a0 = -a0 + 1
                else:
                    a0 = -a0
            else: # reset a0 and increment a1
                a0 = 1
                if a1 > 0:
                    a1 = -a1
                else:
                    a1 = -a1 + 1
    print("Fp{} = Fp[x]/({})".format(e, x**e + a1*x - a0))
    Fpe = Fp.extension(z**e + a1*z - a0, names=('i',)); (i,) = Fpe._first_ngens(1)
    Fp14 = Fpe
    Fq = Fpe
    Fp14s = Fp14['s']; (s,) = Fp14s._first_ngens(1)
    xiM, atwM, btwM = find_twist_curve_parameter_xi_ab(a, Fq, r, c2, d=2, D_twist=False, ba=b, find_mult_i=False)
    EM = EllipticCurve(Fq, [Fq(atwM), Fq(btwM)])

    Fq2M = Fq.extension(s**2 - xiM, names=('wM',)); (wM,) = Fq2M._first_ngens(1)
    Eq2M = EllipticCurve([Fq2M(a), Fq2M(b)])

    try:
        coeffs_xiM = xiM.polynomial().list()
    except AttributeError as err:
        coeffs_xiM = xiM.list()
    i0M = coeffs_xiM[0]
    i1M = coeffs_xiM[1]
    i0m = ZZ(i0M)
    if abs(i0m - p) < abs(i0m):
        i0m = i0m - p
    i1m = ZZ(i1M)
    if abs(i1m - p) < abs(i1m):
        i1m = i1m - p
    if i0m == 0:
        str_xiM = ""
    else:
        str_xiM = "{}".format(i0m)
    if i1m == 1:
        if len(str_xiM) == 0:
            str_xiM = "i"
        else:
            str_xiM += "+i"
    elif i1m == -1:
        str_xiM += "-i"
    elif i1m != 0:
        if len(str_xiM) == 0:
            str_xiM = "{}*i".format(i1m)
        else:
            str_xiM += "{:+}*i".format(i1m)
    print("M-twist xiM = {}".format(str_xiM))
    # xi = i0 + i*i1
    # s^d - xi = 0 <=> s^d - i0 = i1*i <=> (s^d - i0)^e = i1^e*(i^e) where here i^e + a1*i - a0 = 0
    #                                  <=> (s^d - i0)^e = i1^e*(-a1*i + a0) and where i = (s^d-i0)/i1
    # resultant(s^d-xi, z^e - a0)
    # resultant(s^d-xi, z^e +a1*z - a0)
    poly_M = Fpz((z**d - i0M)**e - i1M**e * (a0-a1*(z**d-i0M)/i1M))
    assert poly_M.is_irreducible()
    FpkM = Fp.extension(poly_M, names=('SM',)); (SM,) = FpkM._first_ngens(1)
    EkM = EllipticCurve(FpkM, [FpkM(a), FpkM(b)])
    
    def map_Fq2M_FpkM(x, aM=None):
        if aM is None:
            # evaluate elements of Fq2M = Fp[i]/(i^e+a1*i-a0)[s]/(s^2-xiM) at i=(S^2-i0)/i1 and s=S
            return sum([xi.polynomial()((SM**2-i0M)/i1M) * SM**ei for ei,xi in enumerate(x.list())])
        else:
            return sum([xi.polynomial()((aM**2-i0M)/i1M) * aM**ei for ei,xi in enumerate(x.list())])

    def map_Fq_FpkM(x):
        # evaluate elements of Fq=Fp[i] at i0+i1*i=wM^2 <=> i = (wM^2 - i0)/i1
        return x.polynomial()((SM**2-i0M)/i1M)

    # test Frobenius power
    if (p % 14) == 3:
        m = Fp14.random_element()
        m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13 = m.polynomial().list()
        assert m**p == m.frobenius()
        c0 = (p-3)//14
        d1 = (-a1*i + a0)**c0
        d2 = d1**2
        d3 = d1**3
        d4 = d1**4*(-a1*i+a0)
        d5 = d1**5*(-a1*i+a0)
        d6 = d1**6*(-a1*i+a0)
        d7 = d1**7*(-a1*i+a0)
        d8 = d1**8*(-a1*i+a0)**2
        d9 = d1**9*(-a1*i+a0)**2
        d10 = d1**10*(-a1*i+a0)**2
        d11 = d1**11*(-a1*i+a0)**2
        d12 = d1**12*(-a1*i+a0)**2
        d13 = d1**13*(-a1*i+a0)**2
        assert m**p == m0 + m1*d1*i**3 + m2*d2*i**6 + m3*d3*i**9 + m4*d4*i**12 + m5*d5*i + m6*d6*i**4 + m7*d7*i**7 + m8*d8*i**10 + m9*d9*i**13 + m10*d10*i**2 + m11*d11*i**5 + m12*d12*i**8 + m13*d13*i**11
        print("p = 3 mod 14, checked Frobenius power")

    print("test E (G1)")
    test_order(E,r*c)

    P = E.random_element()
    cP = c*P
    while cP.is_zero():
        P = E.random_element()
        cP = c*P
    psi_g1 = psi_g1_gg28d11(cP,lambda_mod_r)
    if psi_g1 is not None:
        print("test subgroup membership testing: u= {} mod {}".format(u0 % m, m))
        test_membership_testing_g1(E, r, c, u0, t, psi_g1, subgroup_membership_testing_g1_gg28d11, False)
    else:
        print("test subgroup membership testing")
        print("***!!! no psi found to match the eigenvalue, u0 = {} mod {}".format(u0 % m, m))

    print("test E' (G2) M-twist")
    test_order(EM,r*c2)

    print("test Frobenius map on G2 with M-twist")
    test_g2_frobenius_eigenvalue(EkM, EM, Fq2M, map_Fq2M_FpkM, r, c2, D_twist=False)
    test_g2_frobenius_eigenvalue_alt(EkM, EM, map_Fq_FpkM, r, c2, D_twist=False)

    #print("test GLV on G1") # this function is defined in test_scalar_mult.py
    #test_glv_scalar_mult_g1_j_3375(E, lambda_mod_r, beta_mod_p, r, c)

    print("\nFinal exponentiation")
    test_final_exp_easy_k28(FpkM)
    #test_final_exp_hard_k28(FpkM, u0, r, final_exp_hard_gg28d11, expected_exponent=ee1)

    print("test Miller M-twist tate")
    test_miller_function_tate(E, Eq2M, EM, r, c, c2, D_twist=False)
    test_miller_function_tate_2naf(E, Eq2M, EM, r, c, c2, D_twist=False)

    print("test Miller M-twist ate")
    test_miller_function_ate(E, Eq2M, EM, r, c, c2, t-1, D_twist=False)
    test_miller_function_ate_2naf(E, Eq2M, EM, r, c, c2, t-1, D_twist=False)

    print("test Miller M-twist optimal ate")
    assert ((u0**2 + u0*p + 3*p**2) % r) == 0
    test_bilinearity_miller_loop_ate_absolute_extension(E, EM, Fq2M, FpkM, map_Fq2M_FpkM, r, c, c2, u0, D_twist=False, function_name=miller_loop_opt_ate_gg28d11_v0, curve_a_is_0=False)
    test_bilinearity_miller_loop_ate_absolute_extension(E, EM, Fq2M, FpkM, map_Fq2M_FpkM, r, c, c2, u0, D_twist=False, function_name=miller_loop_opt_ate_gg28d11_v1, curve_a_is_0=False)
    test_bilinearity_miller_loop_ate_absolute_extension(E, EM, Fq2M, FpkM, map_Fq2M_FpkM, r, c, c2, u0, D_twist=False, function_name=miller_loop_opt_ate_gg28d11, curve_a_is_0=False)

"""
Seed for the tests.
There are very few seeds for small r.
test_vector_GG28 = [
    {'u':0x21d428, 'u_mod_14':2, 'a':-264, 'b':-1694, 'pnbits':608,'rnbits':436, 'cofact_r':1992111269, 'deg_h_S':None,'cost_S':None, 'label':"u=+2^21+2^17-2^14+2^12+2^10+2^5+2^3  Hw2NAF=7"},#0
]
"""

def test_curve_608_r436():
    """
    the smallest possible positive seed for this family such that r has a factor that is known to be prime
    """
    u0 = ZZ(0x21d428)
    assert u0 == ZZ(2**21+2**17-2**14+2**12+2**10+2**5+2**3)
    a = -264
    b = -1694
    test_curve(u0, a, b)

if __name__ == "__main__":
    arithmetic(False)
    test_curve_608_r436()

