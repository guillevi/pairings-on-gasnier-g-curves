"""
author: Aurore Guillevic
email: aurore.guillevic@inria.fr
created: 2024, December 15
updated: 

Implementation of pairing on Gasnier-Guillevic new curve with k=28 and D=11

(x^2 +x*qx + 3*qx^2) % rx == 0
"""

from pairing import *
from sage.rings.integer_ring import ZZ

def miller_loop_opt_ate_gg28d11_v0(Q, P, a, u):
    """Miller loop with formula u^2 +u*q + 3*q^2 = 0 mod r
    f_{u^2 + u*q + 3*q^2, Q}(P)
    INPUT:
    - `Q`: point on E(Fpk) of order r
    - `P`: point on E(Fp), E in short Weierstrass y^2 = x^3 + a*x + b
    - `a`: curve coefficent, y^2 = x^3 + a*x + b
    - `u`: seed for curve parameters
    """
    q = (P[0]).parent().characteristic()
    v = u**2 + u*q + 3*q**2
    m2, u2Q = miller_function_ate(Q, P, a, v, m0=1)
    
    return m2

def miller_loop_opt_ate_gg28d11_v1(Q, P, a, u):
    """Miller loop with formula u^2 + u*q + 3*q^2 = 0 mod r
    f_{u^2,Q}(P) * f_{u, pi(Q)}(P) * l_{[u^2]Q, [u]pi(Q)}(P) * f_{3, Q}(P)^q^2
    INPUT:
    - `Q`: point on E(Fpk) of order r
    - `P`: point on E(Fp), E in short Weierstrass y^2 = x^3 + a*x + b
    - `a`: curve coefficent, y^2 = x^3 + a*x + b
    - `u`: seed for curve parameters
    """
    m0, _3Q = miller_function_ate(Q, P, a, 3, m0=1)
    m2, u2Q = miller_function_ate(Q, P, a, u**2, m0=1)
    q_Q = (Q[0].frobenius(), Q[1].frobenius())
    m1, u_qQ = miller_function_ate(q_Q, P, a, u, m0=1)
    l3, S = add_line_j_with_z(u2Q, u_qQ, (P[0], P[1]))
    
    return m2 * m1 * l3 * m0.frobenius(2)

def miller_loop_opt_ate_gg28d11(Q, P, a, u):
    """Miller loop with formula u^2 + u*q + 3*q^2 = 0 mod r
    f_{u,Q}(P)^u * f{u, [u]Q}(P) * f_{u, Q}(P)^q * l_{[u^2]Q, [u]pi(Q)}(P) * f_{3, Q}(P)^q^2
    where f_{3, Q}(P) = l_{Q,Q}(P)^2 * l_{2Q,Q}(P)
    INPUT:
    - `Q`: point on E(Fpk) of order r
    - `P`: point on E(Fp), E in short Weierstrass y^2 = x^3 + a*x + b
    - `a`: curve coefficent, y^2 = x^3 + a*x + b
    - `u`: seed for curve parameters

    TODO: it should be possible to share the costs of the first doubling of f_{u, Q}(P) and f_{2, Q}(P)
    """
    u0 = u
    m1, uQ = miller_function_ate(Q, P, a, u0, m0=1)
    Z = 1/uQ[2]
    Z2 = Z**2
    Z3 = Z2 * Z
    uQ = (uQ[0]*Z2, uQ[1]*Z3) # affine coordinates from Jacobian coordinates
    if u < 0:
        mu = m1.conjugate()
    else:
        mu = m1
    m2, u2Q = miller_function_ate(uQ, P, a, u0, m0=mu)
    u_qQ = (uQ[0].frobenius(), uQ[1].frobenius())
    l3, S = add_line_j(u2Q, u_qQ, (P[0], P[1]))
    l2, _2Q = double_line_affine_j((Q[0], Q[1]), (P[0], P[1]), a)
    m3, _3Q = add_line_j(_2Q, (Q[0], Q[1]), (P[0], P[1]))

    return m2 * m1.frobenius() * l3 * (l2 * m3).frobenius(2)

def psi_g1_gg28d11(P, lambda_mod_r):
    """
    return the curve endomorphism that has eigenvalue lambda_mod_r applied to the point P (which should have order r)
    It has eigenvalue lambda = (x**14 + 718)/1079 mod r(x) a root of x^2+x+3 mod r(x)
    Find a point of 3-torsion and compute a 3-isogeny -> it will turn to be a degree-3 endomorphism
    j=-32768
    #a=3*j/(1728-j)
    #b=2*j/(1728-j)
    a = -24/11
    b = -14/11
    E: y^2 = x^3 - 24/11*x -14/11

    K.<w> = NumberField(x^2+x+3)
    (K(-11)).sqrt()
    E = EllipticCurve(K, [-24/11, -14/11])
    E.torsion_points()
    # no point of order 3...
    iso = E.isogenies_prime_degree(3)
    I0 = iso[0]
    I0.codomain().is_isomorphic(E) # True
    I1 = iso[1]
    I1.codomain().is_isomorphic(E) # True
    """
    E = P.curve()
    # find a 3-isogeny to get a degree-3 endomorphism
    iso = E.isogenies_prime_degree(3)
    # select the one that matches the eigenvalue lambda
    ok = False
    i = 0
    while not ok and i < len(iso):
        I = iso[i]
        ok = I.codomain().is_isomorphic(E)
        if ok:
            isom = I.codomain().isomorphism_to(E)
            ok = isom(I(P)) == lambda_mod_r*P
            if not ok:
                ok = isom(I(P)) == -lambda_mod_r*P
                # flip the sign in that case
                psi = -isom*I
            else:
                psi = isom*I
        if not ok:
            i += 1
    if ok:
        assert (psi(psi(P)) + psi(P) + 3*P).is_zero()
        return psi
    return None

def subgroup_membership_testing_g1_gg28d11(P, u, psi_g1=None, lambda_mod_r=None, r=None, c=None):
    """
    test if P belongs to G1 <=> P has order r
    INPUT:
    -`P`: point on an elliptic curve
    -`u`: seed for the curve parameters
    -`lambda_mod_r`: (-1+sqrt(-11))/2 mod r (to check that psi matches)
    # a root of (x^2+x+3) to match Sage isogeny_prime_degree(3)

    The order of G1 is r(x) =
    (x**24 + 5*x**22 + 16*x**20 + 35*x**18 + 31*x**16 - 160*x**14 - 1079*x**12 - 1440*x**10 + 2511*x**8 + 25515*x**6 + 104976*x**4 + 295245*x**2 + 531441)/(3**12 * 13**2 * 83**2)
    The endomorphism is a 3-isogeny translated
    It has eigenvalue lambda = (x**14 + 718)/1079 mod r(x) a root of x^2+x+3 mod r(x)
    A short formula is (a0, a1) s.t. a0 + a1*lambda = (truc)*r
    (-a0+a1*X).resultant(X^2+X+3) = a0^2 + a0*a1 + 3*a1^2 == rx
    """
    if psi_g1 is None and lambda_mod_r is None and (r is None or c is None):
        r = ZZ((x**24 + 5*x**22 + 16*x**20 + 35*x**18 + 31*x**16 - 160*x**14 - 1079*x**12 - 1440*x**10 + 2511*x**8 + 25515*x**6 + 104976*x**4 + 295245*x**2 + 531441)/(3**12 * 13**2 * 83**2))
        c = ZZ(((x**2 + x + 3) * (x**4 - 5*x**2 + 9) * 3**11) // 11)
    if psi_g1 is None and lambda_mod_r is None: # lambda_mod_r needed to compute psi
        lambda_mod_r = ((u**14 + 718)/1079) % r
        # and this should be the one that matches Sage
    if psi_g1 is None:
        psi_g1 = psi_g1_gg28d11(c*P,lambda_mod_r)
    a0 = (253*u**12 + 666*u**10 + 1053*u**8 - 729*u**6 - 13122*u**4 - 59049*u**2 - 177147)//(3**11*13*83)
    a1 = (-160*u**12 + 279*u**10 + 2835*u**8 + 11664*u**6 + 32805*u**4 + 59049*u**2)//(3**12*13*83)
    P0 = a0*P
    P1 = a1*P
    psiP1 = psi_g1(P1)
    Q = P0 - psiP1
    return Q.is_zero()
