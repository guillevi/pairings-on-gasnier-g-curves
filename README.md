# Pairings on Gasnier-Guillevic curves

Implementation of Tate, ate and optimal ate pairings with final exponentiation
on the new KSS-like Gasnier-Guillevic curves.

## See also
The generation of new KSS-like curves is available in SageMath at the
[Subfield Method Project](https://gitlab.inria.fr/jgasnier/subfield-method)
written by Jean Gasnier.

**Preprint**:  
Jean Gasnier, Aurore Guillevic.
_An Algebraic Point of View on the Generation of Pairing-Friendly Curves_.
September 13, 2023.
PDF available at [hal-04205681](https://hal.science/hal-04205681).

## Authors
[Aurore Guillevic](https://members.loria.fr/AGuillevic/), Inria Nancy, France, for the SageMath code.
[Jean Gasnier](https://www.math.u-bordeaux.fr/~jgasnier001/) 
and Aurore Guillevic for the new curves. 

## License
MIT License

